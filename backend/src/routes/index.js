const { Router } = require('express');
const router = Router();

const User = require('../models/User');
const jwt = require('jsonwebtoken');
router.get('/', (req, res) => {
  res.send("rest api")
})

router.post('/signup', async (req, res) => {
  const { email, password } = req.body;
  const newUser = new User({ email, password });

  await newUser.save();

  const token = jwt.sign({ _id: newUser._id }, 'secretkey')
  res.status(200).json({ token })

});

router.post('/signin', async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email })
  if (!user) return res.status(401).send("the mail doesn´t exists");
  if (user.password !== password) return res.status(401).send("Wrong Password");

  const token = jwt.sign({ _id: user._id }, 'secretkey');
  return res.status(200).json({ token });
});
router.get('/tasks', (req, res) => {
  let dat = new Date();
  res.json([
    {
      _id: 1,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 2,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 3,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 4,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 5,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    }
  ])
});
router.get('/private-tasks', verifyToken, (req, res) => {
  let dat = new Date();
  res.json([
    {
      _id: 1,
      name: 'Task One 2',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 2,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 3,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 4,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    },
    {
      _id: 5,
      name: 'Task One',
      description: 'lorem ipsum',
      date: dat
    }
  ])
})

router.get('/profile',verifyToken,(req,res)=>{
  res.send(req.userId);
})

module.exports = router;

function verifyToken(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send('Unthorize Request');
  }
  const token = req.headers.authorization.split(' ')[1];
  if (token === 'null') {
    return res.status(401).send('Unathorize Request');
  }
console.log("sdasdass");

  const payload = jwt.verify(token, 'secretkey');
  req.userId = payload._id;
  next();

}